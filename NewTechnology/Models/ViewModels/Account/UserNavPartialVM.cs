﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewTechnology.Models.ViewModels.Account
{
    public class UserNavPartialVM
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}